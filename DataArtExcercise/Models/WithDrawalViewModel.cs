﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DataArtExcercise.Models
{
    public class WithDrawalViewModel
    {
        public string TemporaryGuid { get; set; }

        [Required(ErrorMessage = "Amount value is required.")]
        [Range(0, 9999999999999999.99)]
        public decimal Amount { get; set; }
    }
}