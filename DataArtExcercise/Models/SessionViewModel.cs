﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataArtExcercise.Models
{
    public class SessionViewModel
    {
        public string TemporaryGuid { get; set; }
        public List<Core.Database.Operations> Operations { get; set; }
    }
}