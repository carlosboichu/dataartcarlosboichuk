﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DataArtExcercise.Models
{
    public class PinInputViewModel
    {
        [Required(ErrorMessage = "Pin number is required.")]
        [StringLength(4, ErrorMessage = "Pin number is a 4 digit value.")]
        [RegularExpression("([0-9 -]+)",
         ErrorMessage = "Characters are not allowed.")]
        public string PinNumber { get; set; }

        public string CardNumber { get; set; }
    }
}