﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DataArtExcercise.Models
{
    public class CardRequestViewModel
    {
        [Required(ErrorMessage = "Card Number is required.")]
        [RegularExpression("([0-9 -]+)",
         ErrorMessage = "Characters are not allowed.")]
        public string CardNumber { get; set; }
    }
}