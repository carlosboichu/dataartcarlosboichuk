﻿using Core.Entities;
using Core.Entities.Enums;
using Core.Services.Implementations;
using Core.Services.Interfaces;
using DataArtExcercise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DataArtExcercise.Controllers
{
    public class HomeController : Controller
    {
        IATMManagmentService atmService;

        public HomeController()
        {
            atmService = new ATMManagmentService();
        }

        public ActionResult Index()
        {
            return RedirectToAction("CardNumberRequest");
        }

        [HttpGet]
        public ActionResult CardNumberRequest()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CardNumberRequest(CardRequestViewModel model)
        {
            if(ModelState.IsValid)
            {
                var resultValidation = atmService.IsCardBlocked(model.CardNumber);

                if (resultValidation == Core.Entities.Enums.enumResults.Ok)
                {
                    return View("PinInput", new PinInputViewModel() { CardNumber = model.CardNumber });
                }
                else if(resultValidation == Core.Entities.Enums.enumResults.CardNumberInexistent)
                {
                    return View("ErrorView", new ErrorViewModel() { Message = "Card Number Inexistent." });
                }
                else if (resultValidation == Core.Entities.Enums.enumResults.Blocked)
                {
                    return View("ErrorView", new ErrorViewModel() { Message = "Your card is blocked." });
                }
                //dbcheck if cardnumber is not blocked go to pin page
              

            }

            return View(model);
        }
 
        [HttpPost]
        public ActionResult PinInput(PinInputViewModel model)
        {
            if (ModelState.IsValid)
            {
                var resultValidation = atmService.ValidatePin(model.CardNumber, model.PinNumber);
                if (resultValidation.result == Core.Entities.Enums.enumResults.Ok)
                {
                    return RedirectToAction("Operations", new { userSession = resultValidation.TemporalId });
                }
                else if (resultValidation.result == Core.Entities.Enums.enumResults.CardNumberInexistent)
                {
                    return View("ErrorView", new ErrorViewModel() { Message = "Card Number Inexistent." });
                }
                else if (resultValidation.result == Core.Entities.Enums.enumResults.Blocked)
                {
                    return View("ErrorView", new ErrorViewModel() { Message = "Your card is blocked." });
                }
                else if (resultValidation.result == Core.Entities.Enums.enumResults.PinInvalid)
                {
                    ViewBag.Message = "Invalid pin number.";
                    return View("PinInput", model);
                }
            }

            return View("PinInput", model);
        }

        public ActionResult Operations(string userSession)
        {
            SessionViewModel model = new SessionViewModel();
            model.TemporaryGuid = userSession;
            model.Operations = atmService.ListOperations(model.TemporaryGuid);
            return View(model);
        }

        public ActionResult Balance(SessionViewModel model)
        {

            Balance result = atmService.GetBalance(model.TemporaryGuid);
            ViewBag.TemporaryGuid = model.TemporaryGuid;
            return View(result);
        }

        public ActionResult WithDrawal(SessionViewModel model)
        {
            WithDrawalViewModel withdrawInfo = new WithDrawalViewModel();
            withdrawInfo.TemporaryGuid = model.TemporaryGuid;
          
            return View(withdrawInfo);
        }

        [HttpPost]
        public ActionResult WithDrawal(WithDrawalViewModel model)
        {
            if (ModelState.IsValid)
            {
                atmService.PerformOperation(model.TemporaryGuid, model.Amount, enumTypeOperation.WithDrawal);
                ViewBag.Message = "WithDrawal succesfull.";
                return RedirectToAction("Operations", new { userSession = model.TemporaryGuid });
            }

            return View(model);
        }
    }
}