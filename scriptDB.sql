USE [atmDb]
GO
/****** Object:  Table [dbo].[Card]    Script Date: 16/07/2018 20:50:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Card](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Number] [nvarchar](32) NULL,
	[Pin] [nvarchar](4) NULL,
	[Attempts] [int] NULL,
	[Username] [nvarchar](50) NULL,
	[Balance] [decimal](18, 2) NULL,
	[Blocked] [bit] NOT NULL,
	[TemporaryGUID] [nvarchar](64) NULL,
	[LastOperationDate] [datetime] NULL,
 CONSTRAINT [PK_Card] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Operations]    Script Date: 16/07/2018 20:50:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Operations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OperationType] [int] NOT NULL,
	[Date] [datetime] NULL,
	[OperationNumber] [nvarchar](50) NULL,
	[Amount] [decimal](18, 2) NULL,
	[CardID] [int] NULL,
 CONSTRAINT [PK_Operations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OperationType]    Script Date: 16/07/2018 20:50:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OperationType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_OperationType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Card] ON 

INSERT [dbo].[Card] ([Id], [Number], [Pin], [Attempts], [Username], [Balance], [Blocked], [TemporaryGUID], [LastOperationDate]) VALUES (1, N'1111-1111-1111-1111', N'1111', 0, N'Carlos', CAST(249000.00 AS Decimal(18, 2)), 0, N'7fe28ab3-046e-475d-9f96-fe01565b43e9', CAST(N'2018-01-01T00:00:00.000' AS DateTime))
INSERT [dbo].[Card] ([Id], [Number], [Pin], [Attempts], [Username], [Balance], [Blocked], [TemporaryGUID], [LastOperationDate]) VALUES (2, N'2111-1111-1111-1111', N'2222', 0, N'Test', CAST(1000.00 AS Decimal(18, 2)), 0, NULL, CAST(N'2018-01-01T00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Card] OFF
SET IDENTITY_INSERT [dbo].[Operations] ON 

INSERT [dbo].[Operations] ([Id], [OperationType], [Date], [OperationNumber], [Amount], [CardID]) VALUES (8, 1, CAST(N'2018-07-16T20:30:34.283' AS DateTime), N'e6095f86-7e69-4253-b89b-1f49694b33f1', CAST(250000.00 AS Decimal(18, 2)), 1)
INSERT [dbo].[Operations] ([Id], [OperationType], [Date], [OperationNumber], [Amount], [CardID]) VALUES (9, 2, CAST(N'2018-07-16T20:41:35.617' AS DateTime), N'd5029553-dfa9-4978-9910-d7dcbe6c47b8', CAST(1000.00 AS Decimal(18, 2)), 1)
INSERT [dbo].[Operations] ([Id], [OperationType], [Date], [OperationNumber], [Amount], [CardID]) VALUES (10, 1, CAST(N'2018-07-16T20:41:41.537' AS DateTime), N'60f44992-c128-489d-8d36-ff366414ea60', CAST(249000.00 AS Decimal(18, 2)), 1)
SET IDENTITY_INSERT [dbo].[Operations] OFF
SET IDENTITY_INSERT [dbo].[OperationType] ON 

INSERT [dbo].[OperationType] ([Id], [Name]) VALUES (1, N'Balance')
INSERT [dbo].[OperationType] ([Id], [Name]) VALUES (2, N'Withdrawal')
SET IDENTITY_INSERT [dbo].[OperationType] OFF
ALTER TABLE [dbo].[Operations]  WITH CHECK ADD  CONSTRAINT [FK_Operations_Card] FOREIGN KEY([CardID])
REFERENCES [dbo].[Card] ([Id])
GO
ALTER TABLE [dbo].[Operations] CHECK CONSTRAINT [FK_Operations_Card]
GO
ALTER TABLE [dbo].[Operations]  WITH CHECK ADD  CONSTRAINT [FK_Operations_OperationType] FOREIGN KEY([OperationType])
REFERENCES [dbo].[OperationType] ([Id])
GO
ALTER TABLE [dbo].[Operations] CHECK CONSTRAINT [FK_Operations_OperationType]
GO
