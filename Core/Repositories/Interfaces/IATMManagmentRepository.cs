﻿using Core.Database;
using Core.Entities;
using Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Services.Interfaces
{
    public interface IATMManagmentRepository
    {
         enumResults IsCardBlocked(string cardNumber);

        ValidationResponse ValidatePin(string cardNumber, string pinNumber);

        List<Database.Operations> ListOperations(string TemporaryGuid);

        Balance GetBalance(string TemporaryGuid);

        void PerformOperation(string TemporaryGuid, decimal Amount, enumTypeOperation OperationType);
    }
}
