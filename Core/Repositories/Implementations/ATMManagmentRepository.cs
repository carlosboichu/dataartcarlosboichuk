﻿using Core.Entities;
using Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Database;
using Core.Entities.Enums;
using System.Globalization;

namespace Core.Services.Implementations
{
    public class ATMManagmentRepository : IATMManagmentRepository
    {
        private atmDbEntities _context;
        
        public ATMManagmentRepository()
        {
            _context = new atmDbEntities();
        }

        public void PerformOperation(string TemporaryGuid, decimal Amount, enumTypeOperation OperationType)
        {
            var result = _context.Card.Where(x => x.TemporaryGUID == TemporaryGuid).FirstOrDefault();
            result.Balance = result.Balance - Amount;
            _context.SaveChanges();

            Database.Operations operation = new Database.Operations();
            operation.CardID = result.Id;
            operation.Date = DateTime.Now;
            operation.OperationNumber = Guid.NewGuid().ToString();
            operation.Amount = Amount;
            operation.OperationType = (int)enumTypeOperation.WithDrawal;

            _context.Operations.Add(operation);
            _context.SaveChanges();
        }

        public Balance GetBalance(string TemporaryGuid)
        {
            var result = _context.Card.Where(x => x.TemporaryGUID == TemporaryGuid).FirstOrDefault();
            Balance balance = new Balance();
            balance.LastOperationDate = new DateTime();
            balance.Amount = result.Balance.Value;
            balance.CardNumber = result.Number;

            Database.Operations operation = new Database.Operations();
            operation.CardID = result.Id;
            operation.Date = DateTime.Now;
            operation.OperationNumber = Guid.NewGuid().ToString();
            operation.Amount = result.Balance;
            operation.OperationType = (int)enumTypeOperation.Balance;

            _context.Operations.Add(operation);
            _context.SaveChanges();

            return balance;
        }

        public enumResults IsCardBlocked(string cardNumber)
        {
           var result = _context.Card.Where(x => x.Number == cardNumber).FirstOrDefault();
        
            if(result != null)
            {
                if(result.Blocked)
                {
                    return enumResults.Blocked;
                }
                return enumResults.Ok;
            }

            return enumResults.CardNumberInexistent;
            
        }

        public List<Database.Operations> ListOperations(string TemporaryGuid)
        {
            var results = _context.Card.ToList();

            var result = _context.Card.Include("Operations").Include("Operations.Card").Include("Operations.OperationType1").Where(x => x.TemporaryGUID == TemporaryGuid).FirstOrDefault();

            return result.Operations.OrderByDescending(y => y.Date).ToList();
        }

        public ValidationResponse ValidatePin(string cardNumber, string pinNumber)
        {
            ValidationResponse response = new ValidationResponse();

            var result = _context.Card.Where(x => x.Number == cardNumber).FirstOrDefault();
            if (result != null)
            {
                if(result.Blocked)
                {
                    response.result =  enumResults.Blocked;
                }
                else if (result.Pin == pinNumber)
                {
                    result.Attempts = 0;
                    result.TemporaryGUID = Guid.NewGuid().ToString();
                    _context.SaveChanges();
                    response.TemporalId = result.TemporaryGUID;
                    response.result = enumResults.Ok;
                }
                else if(result.Attempts < 4)
                {
                    result.Attempts += 1;
                    _context.SaveChanges();
                    response.result = enumResults.PinInvalid;
                }
                else if (result.Attempts == 4)
                {
                    result.Blocked = true;
                    _context.SaveChanges();
                    response.result = enumResults.Blocked;
                }

                return response;
            }

            response.result = enumResults.CardNumberInexistent;

            return response;
        }
    }
}
