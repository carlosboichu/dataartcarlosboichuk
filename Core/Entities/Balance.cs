﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class Balance
    {
        public DateTime LastOperationDate { get; internal set; }
        public decimal? Amount { get; internal set; }
        public string CardNumber { get; internal set; }
    }

}
