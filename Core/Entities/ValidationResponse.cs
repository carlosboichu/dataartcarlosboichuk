﻿using Core.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class ValidationResponse
    {
        public enumResults result { get; set; }

        public string TemporalId { get; set; }
    }
}
