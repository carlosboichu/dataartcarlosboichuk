﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entities.Enums
{
    public enum enumResults
    {
        Blocked = 1,
        CardNumberInexistent = 2,
        Ok = 3,
        PinInvalid = 4
    }
}
