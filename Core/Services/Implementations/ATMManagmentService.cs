﻿using Core.Entities;
using Core.Entities.Enums;
using Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Services.Implementations
{
    public class ATMManagmentService : IATMManagmentService
    {
        private IATMManagmentRepository repository;

        public ATMManagmentService()
        {
            repository = new ATMManagmentRepository();
        }

        public ATMManagmentService(IATMManagmentRepository atmRepository)
        {
            repository = atmRepository;
        }

        public Balance GetBalance(string TemporaryGuid)
        {
            return repository.GetBalance(TemporaryGuid);
        }

        public enumResults IsCardBlocked(string cardNumber)
        {
            return repository.IsCardBlocked(cardNumber);
        }

        public List<Database.Operations> ListOperations(string TemporaryGuid)
        {
            return repository.ListOperations(TemporaryGuid);
        }

        public void PerformOperation(string TemporaryGuid, decimal Amount, enumTypeOperation OperationType)
        {
            repository.PerformOperation(TemporaryGuid, Amount, OperationType);
        }

        public ValidationResponse  ValidatePin(string cardNumber, string pinNumber)
        {
            return repository.ValidatePin(cardNumber, pinNumber);
        }
 
    }
}
